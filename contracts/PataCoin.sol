// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract PataCoin is ERC20, ERC20Burnable{
  constructor(uint256 _supply) ERC20("PataCoin","PTC") {
    _mint(msg.sender, _supply * (10 ** decimals()));
  }

  function decimals() public view virtual override returns (uint8) {
    return 18;
  }
}
